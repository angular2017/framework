import { Component, OnInit } from '@angular/core';
import { MyDataService } from './my-data.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Tour of Heroes';
  heroes = ['Windstorm', 'Bombasto', 'Magneta', 'Tornado'];
  myHero = this.heroes[0];
  title3 = 'Galley works';
  title4 = 'Latest photogrophe';

  constructor(private newService: MyDataService) {}

 ngOnInit() {
      this.newService.Obj.name = 'aidan';
     console.log(this.newService.success() );
     console.log(this.newService.Obj.name);
  }

}
